function visitLink(path) {
  const count = +localStorage.getItem(path);
  if (!count) {
    localStorage.setItem(path, "1");
    return 0;
  }
  localStorage.setItem(path, String(++count));
  return 0;
}

let divver;
let ul;

window.addEventListener("load", () => {
  divver = document.getElementById("content");
  ul = document.createElement("ul");
  divver.appendChild(ul);
});

function viewResults() {
  if (ul.hasChildNodes()) {
    ul.innerHTML = "";
  }

  const page1 = localStorage.getItem("Page1") || "0";
  const page2 = localStorage.getItem("Page2") || "0";
  const page3 = localStorage.getItem("Page3") || "0";

  let li1 = document.createElement("li");
  let li2 = document.createElement("li");
  let li3 = document.createElement("li");

  let text1 = document.createTextNode(`You visited Page1 ${page1} time(s)`);
  let text2 = document.createTextNode(`You visited Page2 ${page2} time(s)`);
  let text3 = document.createTextNode(`You visited Page3 ${page3} time(s)`);

  li1.appendChild(text1);
  li2.appendChild(text2);
  li3.appendChild(text3);

  ul.appendChild(li1);
  ul.appendChild(li2);
  ul.appendChild(li3);
  localStorage.clear();
}
